#oschat

    oschat基于HTML5 WebSocket的开源IM，后台服务基于tomcat7.0.42 + akka. 客户端使用了node-webkit，使用html5+js进行开发. 客户端完全用了web相关技术，所有很方便的进行二次开发和集成.
    
    当前版本 0.0.1-SNAPSHOT
    

##说明

* 服务器使用了 Tomcat 7.0.42 , 使用Akka 作为消息的转发容器...（待补充）
* 请使用 支持 WebSocket 的浏览器试用！！！


##如何部署服务

1. 通过Git clone或者直接下载代码的压缩包获取代码。
2. 安装Maven，讲maven配置到系统环境变量中，通过控制台进入工程目录，在包含 pom.xml 的目录下，输入: mvn package 
3. 通过maven打包后，会生成target文件夹，讲target中的 oschat.war 复制到tomcat7.0.42中运行。
4. 如何将maven工程导入eclipse中？
    在pom.xml所在的目录下，控制台中输入:mvn eclipse:eclipse  转化为eclipse工程
    为eclipse安装scala2.10插件。 [Scala IDE for Eclipse](http://scala-ide.org/download/current.html) 根据eclipse的版本安装对应的插件

##客户端

* 客户端使用了 [node-webkit](https://github.com/rogerwang/node-webkit) 进行开发，支持跨平台。
* client/code 文件中，放了node-webkit的windows客户端，可直接运行nw.exe打开。
* 更多node-webkit说明，访问 https://github.com/rogerwang/node-webkit