

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `MEMBER`
-- ----------------------------
DROP TABLE IF EXISTS `MEMBER`;
CREATE TABLE `MEMBER` (
  `ID` varchar(254) NOT NULL,
  `NICKNAME` varchar(254) NOT NULL,
  `USERNAME` varchar(254) NOT NULL,
  `PASSWORD` varchar(254) NOT NULL,
  `CREATETIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `PORTRAIT` varchar(255) DEFAULT NULL,
  `RESUME` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of MEMBER
-- ----------------------------
INSERT INTO `MEMBER` VALUES ('6a5100a26e7d4cdf8d5f0d0d39718834', 'admin', 'admin', '11111', '2013-08-26 19:26:35', 'http://static.oschina.net/uploads/user/127/254662_100.jpg', null);
INSERT INTO `MEMBER` VALUES ('254662', 'ForEleven', '254662', '254662', '2013-08-26 16:57:22', 'http://static.oschina.net/uploads/user/127/254662_100.jpg', '');
