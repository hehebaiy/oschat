package me.feng.domain

import scala.slick.direct.AnnotationMapper.table
import me.feng.db.DbHelper
import scala.slick.driver.MySQLDriver.simple._
import java.sql.Timestamp
import java.util.logging.Logger
import java.util.UUID
import java.util.Date
import Database.{ threadLocalSession => session }
import me.feng.entity.User
import scala.slick.jdbc.{ GetResult, StaticQuery => Q }
import com.mchange.v2.c3p0.ComboPooledDataSource
import scala.slick.jdbc.GetResult
import scala.slick.direct.AnnotationMapper.column
import scala.slick.direct.AnnotationMapper.table

@table("MEMBER") case class Member(id: String, nickname: String, username: String, password: String, cratetime: Timestamp,portrait:String,resume:String)

object Member extends Table[(String, String, String, String, Timestamp,String,String)]("MEMBER") with DbHelper {

  val log = Logger.getLogger(this.getClass().getName())

  def id = column[String]("ID", O.PrimaryKey)
  def nickname = column[String]("NICKNAME")
  def username = column[String]("USERNAME")
  def password = column[String]("PASSWORD")
  def cratetime = column[Timestamp]("CREATETIME")
  def portrait = column[String]("PORTRAIT")
  def resume = column[String]("RESUME")

  def * = id ~ nickname ~ username ~ password ~ cratetime ~ portrait ~ resume

  implicit val getMemberResult = GetResult(r => Member(r.<<, r.<<, r.<<, r.<<, r.<<,r.<<, r.<<))

  def initTable() = withSession {
    val ddl = Member.ddl
    ddl.createStatements.foreach(log.info)
    ddl.create
    Member.insert(UUID.randomUUID().toString().replace("-", ""), "admin", "admin", "11111", new Timestamp(new Date().getTime()),"","")
  }

  def getMemberbyUsername(username: String): Member = withSession {
    val q = Q[String, Member] + "select * from MEMBER where username=?"
    val l = q(username).list
    if(l.size>0) l(0) else null
  }
  
  def getMemberbyUid(uid:String):Member = withSession { 
    val q = Q[String, Member] + "select * from MEMBER where id=?"
    val l = q(uid).list
    if(l.size>0) l(0) else null
  }
  
  def save(m:Member) :Member = withSession {
    Member.insert(m.id,m.nickname,m.username,m.password,m.cratetime,m.portrait,m.resume)
    m
  }

}