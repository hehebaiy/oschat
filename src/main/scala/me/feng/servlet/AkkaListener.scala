package me.feng.servlet

import java.util.logging.Logger
import akka.actor.ActorSystem
import javax.servlet.ServletContextEvent
import javax.servlet.ServletContextListener
import javax.servlet.annotation.WebListener
import me.feng.actors.MainActor
import akka.actor.Props
import me.feng.OschartContext
import me.feng.db.DbHelper

@WebListener
class AkkaListener extends ServletContextListener {

  private val logger = Logger.getLogger(classOf[AkkaListener].getName());
  val actorSystem: ActorSystem = ActorSystem()

  override def contextInitialized(event: ServletContextEvent) {
    
    logger.info("=== Akka system Starting");

    val main = actorSystem.actorOf(Props[MainActor], "main")
    event.getServletContext().setAttribute("akka", actorSystem)
    event.getServletContext().setAttribute("mainActor", main)
  }

  override def contextDestroyed(event: ServletContextEvent) {
    event.getServletContext().removeAttribute("akka")
    event.getServletContext().removeAttribute("mainActor")
    logger.info("=== Akka system shutting down");
    //OschartContext.destroy()
    try {
      actorSystem.shutdown()
      Thread.sleep(1000);
    }
    logger.info("=== Akka system shutdown complete");
    
  }

}