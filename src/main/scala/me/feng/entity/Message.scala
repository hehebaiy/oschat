package me.feng.entity

import java.util.Date
import scala.beans.BeanProperty
import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonProperty
import scala.annotation.meta.getter
import scala.annotation.meta.param


/**
 * code : 消息类型用来判断  result的数据结构
 *        0:Event   1:Letter
 */
case class Message[T] ( kind:MessageType.Value, result:T, time:Date = new Date)

case class Msg @JsonCreator()(@JsonProperty("t") t:Int,@JsonProperty("id") id:String,@JsonProperty("m") msg:String)

object MessageType extends Enumeration{
  type MessageType = Value
  var EVENT = Value(0)
  var LETTER = Value(1)
}