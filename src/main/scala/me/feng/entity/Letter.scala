package me.feng.entity

case class Letter(id:String, from:User, cid:String, msg:String,var kind:LetterType.Value)

object LetterType extends Enumeration {

  type LetterType = Value
  val MAIN,ROOM, USER, NEWCHAT = Value
}