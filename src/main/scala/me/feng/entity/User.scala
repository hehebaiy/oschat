package me.feng.entity

import me.feng.domain.Member

case class User(id: String, name: String,portrait:String,resume:String)

object User {

  def member2User(m: Member): User = {
    new User(m.id, m.nickname,m.portrait,m.resume)
  }
}

