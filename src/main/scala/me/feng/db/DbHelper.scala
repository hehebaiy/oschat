package me.feng.db

import com.mchange.v2.c3p0.ComboPooledDataSource
import javax.sql.DataSource
import java.util.logging.Logger
import scala.slick.session.Database
import me.feng.domain.Member

trait DbHelper {

  private val logger = Logger.getLogger(this.getClass().getName());
  logger.info("Created c3p0 connection pool")
  var cpds: ComboPooledDataSource = new ComboPooledDataSource
  
  lazy val database = Database.forDataSource(cpds)

  def withSession[T](f: => T): T = database.withSession(f)

}