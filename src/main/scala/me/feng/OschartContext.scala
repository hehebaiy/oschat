package me.feng


import akka.actor.ActorSystem
import akka.actor.Props
import me.feng.actors.MainActor
import me.feng.messageinbound.MainChart

class OschartContext {
  

}

object OschartContext {
  private val actorSystem: ActorSystem = ActorSystem()
  private val context: OschartContext = new OschartContext

  var akka : ActorSystem = _

  def getContext() = context

  def initContext() = {
    actorSystem.actorOf(Props[MainActor], "main")
  }
  
  def getMainActor = actorSystem.actorFor("main")
  
  def getUserActor(uid:String) = {
    var actor = actorSystem.actorFor(uid)
    actor
  }

  def getActorSystem() = actorSystem

  def destroy() = actorSystem.shutdown()
}