var oschat = {
	version: "0.0.1"
};

var data = {
	osclogin :false
};


oschat.win = (function(){
	return {
		init: function(){
			oschat.win.viewEventBind();
			
			nw.login.init();
		},
		
		viewEventBind:function(){
			$('.login-btn').click(function(){
				var username = $("#username").val();
				var password = $("#password").val();
				if(username==""){
					$('.login-msg span').html('用户名不能为空！');
					$('.login-msg').show();
				}else{
					if(data.osclogin){
						api.loginosc(username, password,function(data){
							oschat.win.loginCallBack(data);
						});
					}else{
						api.login(username, password, function(data){
							oschat.win.loginCallBack(data);
						});
					}
				}
			});

			$('.login-osc').click(function() {
				$("#username")[0].placeholder="osc 帐号";
				data.osclogin = !data.osclogin;
				$('#osclogin')[0].checked = data.osclogin;
			});

			$('#win_close_btn').click(function(){
				nw.win.close();
			});

			$('#win_min_btn').click(function(){
				nw.win.minimize();
			});
		},

		loginCallBack:function(data){
			var result = data.result;
			if(result == "CONNECTION_FAIL"){
				$('.login-msg span').html('服务器连接失败！');
				$('.login-msg').show();
				return;
			}
			if(result.event == "LOGIN_SUCCESS"){
				nw.win.window.location.href = "main.html?uid="+result.u.id;
				nw.win.resizeTo(900,600);
			}else{
				$('.login-msg span').html('用户名或者密码错误！');
				$('.login-msg').show();
			}
		},
		
		winEventBind : function(){
			nw.win.on('resize',function(){
				alert(1);
			});
		}
	}
})();