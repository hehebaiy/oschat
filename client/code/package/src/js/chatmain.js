
var chatmain={
	ulist : new Array()
};

chatmain.win = (function(){
	return {
		
		init:function(){
			chatmain.win.viewEventBind();
		},
		
		viewEventBind:function(){
			$('.chat-u').each(function(){
				$(this).click(function(){
					chatmain.win.userClickBind(this);
				});
			});
		},
		
		userClickBind:function(div){
			$('.chat-u-active').removeClass('chat-u-active');
			$(div).addClass('chat-u-active');
			$('.chat-frame').each(function(){$(this).hide();});
			var id = $(div).attr("id");
			$('#frame-'+id).show();
			$('#'+id+' .msg-count').html(0);
			$('#'+id+' .msg-count').hide();
		},
		
		addMsgCount:function(uid){
			var b = $('#chat-'+uid).hasClass('chat-u-active');
			if(!b){
				var count = $('#chat-'+uid+' .msg-count').html();
				if(count == "99+"){
					return;
				}else if(count=="99"){
					count= count+"+";
				}else{
					count = parseInt(count)+1;
				}
				$('#chat-'+uid+' .msg-count').html(count);
				$('#chat-'+uid+' .msg-count').show();
			}
		},
		
		/**
		 * <div class="chat-u chat-u-active">
				<img width="25" height="25" src="http://static.oschina.net/uploads/user/89/179699_50.jpg">
				<div class="u-name">铂金小猪<div class="msg-count">10+</div></div>
			</div>
		 */
		addChat:function(u){
			var id = u.id;
			if($('#chat-'+id).length>0){
				$('.chat-u-active').removeClass('chat-u-active');
				$('.chat-frame').each(function(){$(this).hide();});
				$('#chat-'+id).addClass('chat-u-active');
				$('#frame-chat-'+id).show();
			}else{
				var portrait = u.portrait == null ? "" :  u.portrait;
				var name = u.name;
				var html = '<div id="chat-'+id+'" class="chat-u chat-u-active"><img width="25" height="25" src="'+portrait+'"><div class="u-name">'+name+'<div class="msg-count">0</div></div></div>';
				$('.chat-u-active').removeClass('chat-u-active');
				$('.chat-u-list').append(html);
				
				$('.chat-frame').each(function(){$(this).hide();});
				var chathtml='<div class="chat-frame" id="frame-chat-'+id+'"><iframe id="chatFrame" name="chatFrame" src="chat.html?u='+chatmain.ulist.length+'"  frameborder="0" height="100%" width="100%" ></iframe></div>';
				$('.chatmain').append(chathtml);
				
				// Bind click event
				$('#chat-'+id).click(function(){
					chatmain.win.userClickBind(this);
				});
				
				chatmain.ulist[chatmain.ulist.length] = u;
			}
		}
	};
})();