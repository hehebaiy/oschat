
var im = {
	ws:null,
	mainws : "ws://localhost:8080/main.ws",
	chatws : "ws://localhost:8080/chat.ws"
};

im.api = (function(){
	return {
		initMainWs : function(uid){
			if ('WebSocket' in window) { 
				im.ws = new WebSocket(im.mainws+"?uid="+uid);
			} else if ('MozWebSocket' in window) { 
				im.ws = new MozWebSocket(url); 
			} else { 
				console.error("初始化 Main websocket 对象失败！");
				return; 
			}
		},
		initChatWs : function(uid,cid){
			if ('WebSocket' in window) { 
				im.ws = new WebSocket(im.chatws+"?uid="+uid+"&cid="+cid);
			} else if ('MozWebSocket' in window) { 
				im.ws = new MozWebSocket(url); 
			} else { 
				console.error("初始化 Chat websocket 对象失败！");
				return; 
			}
		}
	};
})();